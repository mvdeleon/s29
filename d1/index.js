
// Comparison Query Operators
/*
	> greater than- $gt
		db.collectionName.find({field: {$gt: value}})
		db.users.find({age: {$gt: 76}});
	
	< less than: $lt
		db.collectionName.find({field: {$lt: value}})
		db.users.find({age: {$lt: 65}});

	>= greater than or equal to: $gte
		db.collectionName.find({field: {$gt: value}})
		db.users.find({age: {$gte: 76}});

	<= less than or equal to: $lte
		db.collectionName.find({field: {$lte: value}})
		db.users.find({age: {$lte: 65}});

	!= not equal to: $ne
		db.collectionName.find({field: {$ne: value}})
		db.users.find({age: {$ne: 65}});

	in $in
		db.collectionName.find({field: {$in: value}})
		db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
		db.users.find({courses: {$in: ["HTML", "React"]}});

*/

/* EVALUATION QUERY OPERATORS

	Regex: $regex
	Case sensitive query
		db.collectionName.find({field: {$regex: pattern}});
		db.users.find({lastName: {$regex: 'A'}});

	Case insensitive query - "i" option
		db.collectionName.find({field: {$regex: pattern, $options: $optionValue}});
		db.users.find({lastName: {$regex: 'A', $options: '$i'}});


*/


db.users.find({firstName: {$regex: 'E', $options: '$i'}});

/* LOGICAL QUERY OPERATORS
	db.users.find({$or: [{fieldA: "ValueA"}, {fieledB: "ValueB"}]});

	Or: $or

*/

db.users.find({$or: [{firstName: "Neil"}, {age: "25"}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt:30}}]});

//  And: $and
//  	db.users.find({$and: [{fieldA: "ValueA"}, {fieledB: "ValueB"}]});

	db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});
	db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});



// Activity

	db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

	db.users.find({$and: [{firstName: {$regex: "e", $options: '$i'} }, {age: {$ne: 30}}]});


/* FIELD Projection

	INCLUSION
		db.collectionName.find({criteria}, {field:1}});
	
*/

db.users.find({ firstName: "Jane"})

db.users.find({ firstName: "Jane"},{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
}

	)

/* Embeded documents = double quotes for sub or embeded objects (use dot. example:
	contact:
		phone
		address

	contact.phone - to show phone only
)

*/

db.users.find({ firstName: "Jane"})

db.users.find({ firstName: "Jane"},{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
}

	)



/* Exception

	db.collectionName.find({criteria}, {_id:0}});


*/

db.users.find({ firstName: "Jane"},{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id:0
}

	);


/* SLICE - $slice


*/

db.users.insert({
	namearr: [
		{
			namea: "Juan"
		},
		{
			nameb: "tamad"
		}
	]
});
db.users.find({
	namearr:
	{
			namea: "Juan"
		}
})

// 

db.users.insert({
	"namearr": [
		{
			namea: "Juan"
		},
		{
			namearr: {$slice: 1}
		}]
	
});
db.users.find({
	namearr:
	{
			namea: "Juan"
		}
})

// ACTIVITY

db.users.find(
	{firstName: {$regex: 's', $options: '$i'}}, 
	{
	firstName: 1,
	lastName: 1,
	_id:0
	}

);




// Exclusion


db.users.find({ firstName: "Jane"},{
	
	contact: 0,
	department: 0,
	
}

	);


// Excluding specific or embeded

db.users.find({ firstName: "Jane"},{
	
	"contact.phone": 0,
	
	
}

	);